package com.example.pogodynka;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.annotations.SerializedName;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TheWeather extends AppCompatActivity {

    private SwipeRefreshLayout swipeRefreshLayout;
    private String icon;

    //---------------- TO MY APPLICATION
    private TextView cityView;
    private TextView hourView;
    private TextView tempView;
    private TextView presView;
    private TextView tempMinView;
    private TextView humView;
    private TextView tempMaxView;
    private String city;
    private ImageView imageView;
    //------------------
    final Handler handler = new Handler();
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_the_weather);
        cityView=findViewById(R.id.cityView);
        hourView=findViewById(R.id.hourView);
        presView=findViewById(R.id.presView);
        tempView=findViewById(R.id.tempView);
        humView=findViewById(R.id.humView);
        tempMinView=findViewById(R.id.tempMinView);
        tempMaxView=findViewById(R.id.tempMaxView);
        imageView=findViewById(R.id.imageView);

        intent = getIntent();
        city=intent.getStringExtra("TOWN_TEXT");
        cityView.setText(city);

        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("CITY",city);
        editor.apply();
        cityView.setText(city);
        //-------------------ImageView
            String url="https://openweathermap.org/weather-conditions";

        //------------------SWIPE TO REFRESH


        swipeRefreshLayout = findViewById(R.id.swiperefresh_items);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                cityView.setText(cityView.getText().toString());
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if(swipeRefreshLayout.isRefreshing()) {
                            swipeRefreshLayout.setRefreshing(false);
                            refresh();
                        }
                    }
                },1000);
            }
        });

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                refresh();
                handler.postDelayed(this, 1000);
            }
        }; handler.postDelayed(runnable, 1000);

    }
    private void refresh(){
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm");
        hourView.setText(dateFormat.format(currentTime));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        final JsonPlaceholderAPI jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI.class);

        Call<Weather> call = jsonPlaceholderAPI.getWeather(city+",pl"
                ,"metric"
                ,"749561a315b14523a8f5f1ef95e45864");

        call.enqueue(new Callback<Weather>() {

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onResponse(Call<Weather> call, Response<Weather> response) {
                SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);

                if(!response.isSuccessful()){
                    intent.putExtra("TOWN_TEXT",sharedPreferences.getString("CITY","Tarnów"));
                    MainActivity.getGoThere().setText(sharedPreferences.getString("CITY","Tarnów"));
                    finish();
                    cityView.setText("unknown city");
                    tempView.setText("none");
                    humView.setText("none");
                    presView.setText("none");
                    tempMaxView.setText("none");
                    tempMinView.setText("none");
                    return;
                }
                Weather weather = response.body();
                tempView.setText(Double.toString(weather.getMain().getTemp()) + " °C");
                humView.setText(Double.toString(weather.getMain().getHumidity()) + " %");
                presView.setText(Double.toString(weather.getMain().getPressure()) + " hPa");
                tempMaxView.setText(Double.toString(weather.getMain().getTempMax())+ " °C");
                tempMinView.setText(Double.toString(weather.getMain().getTempMin())+ " °C");


                icon = weather.getWeather()[0].getIcon();
                Picasso.get().setLoggingEnabled(true);
                Picasso.get().load("http://openweathermap.org/img/wn/" + icon + "@2x.png").into(imageView);
                System.out.println("IKONA ID"+icon);




                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("CITY",city);
                editor.apply();
            }
            @Override
            public void onFailure(Call<Weather> call, Throwable t) {
                cityView.setText(t.getMessage());
                tempView.setText("none");
                humView.setText("none");
                presView.setText("none");
                tempMaxView.setText("none");
                tempMinView.setText("none");

            }
        });
    }
}
