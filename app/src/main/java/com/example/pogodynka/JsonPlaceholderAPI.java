package com.example.pogodynka;

import android.graphics.Bitmap;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface JsonPlaceholderAPI {
    @GET("data/2.5/weather") //?q={id},pl&units=metric&APPID=749561a315b14523a8f5f1ef95e45864")
    Call<Weather> getWeather(@Query("q") String city, @Query("units") String units, @Query("APPID") String appid);



}
//http://api.openweathermap.org/data/2.5/weather?q=Wojnicz,pl&units=metric&APPID=749561a315b14523a8f5f1ef95e45864