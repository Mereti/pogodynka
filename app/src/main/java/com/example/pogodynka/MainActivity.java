package com.example.pogodynka;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private static EditText townText;
    private Button checkButton;
    private SwipeRefreshLayout swipeRefreshLayout;


    public static EditText getGoThere(){
        return townText;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        checkButton=findViewById(R.id.checkButton);




        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        townText = findViewById(R.id.townText);
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        String city = sharedPreferences.getString("CITY","Tarnów");
        townText.setText(city);

        }

        public void goThere(View view){
            //-----------------Sprawdzenie sieci------
            int duration = Toast.LENGTH_LONG;
            Context context = getApplicationContext();
            CharSequence text="Internet Connection required!";
            Toast toast = Toast.makeText(context,text,duration);
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if(networkInfo == null ){

                toast.show();
            }else{
            //----------------------------------------

                Intent intent = new Intent(this, TheWeather.class);
                intent.putExtra("TOWN_TEXT", townText.getText().toString());
                startActivity(intent);}
    }


}
